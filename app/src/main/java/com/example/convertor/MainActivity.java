package com.example.convertor;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

@RequiresApi(api = Build.VERSION_CODES.N)
public class MainActivity extends AppCompatActivity {
    private EditText edMeter;
    private EditText edCentimeter;
    private EditText edKilometer;
    private EditText edInch;
    private Button btnConvert, btnReset;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edMeter = findViewById(R.id.edMeter);
        edCentimeter = findViewById(R.id.edCentimeter);
        edKilometer = findViewById(R.id.edKilometer);
        edInch = findViewById(R.id.edInch);
        btnConvert = findViewById(R.id.btnConvert);
        btnReset = findViewById(R.id.btnReset);


        btnConvert.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                float centimeter = 0, meter = 0, inch = 0, kilometer = 0;

                if (edCentimeter.getText().toString().length() > 0) {
                    centimeter = Float.parseFloat(edCentimeter.getText().toString());
                    meter = (centimeter / 100);
                    kilometer = centimeter / 100000f;
                    inch = centimeter / 2.54f;


                } else if (edMeter.getText().toString().length() > 0) {
                    meter = Float.parseFloat(edMeter.getText().toString());
                    centimeter = (meter * 100f);
                    kilometer = meter * 0.001f;
                    inch = meter * 39.3701f;

                } else if (edKilometer.getText().toString().length() > 0) {
                    kilometer = Float.parseFloat(edKilometer.getText().toString());
                    centimeter = (kilometer * 100000);
                    meter = kilometer * 1000;
                    inch = kilometer * 100000 / 2.54f;


                } else if (edInch.getText().toString().length() > 0) {
                    inch = Float.parseFloat(edInch.getText().toString());
                    centimeter = (inch * 2.54f);
                    meter = centimeter / 100;
                    kilometer = centimeter / 100000;


                }
                setText(centimeter, meter, inch, kilometer);


            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();

            }
        })
        ;


    }

    public void reset() {
        edCentimeter.setText("");
        edMeter.setText("");
        edInch.setText("");
        edKilometer.setText("");

    }

    public void setText(float cenitimeter, float meter, float inch, float kilometer) {
        edCentimeter.setText("" + cenitimeter);
        edMeter.setText("" + meter);
        edInch.setText("" + inch);
        edKilometer.setText("" + kilometer);


    }

}
